## Interface: 11302
## Author: Copperbolts
## Version: 0.0.1
## Title: |cff0070DEClassic Stats|r
## Notes: Realm population gathering addon.
## SavedVariables: ClassicStatsDb
## SavedVariablesPerCharacter: ClassicStatsSettings

ClassicStats_Db.lua
ClassicStats_Util.lua
ClassicStats_Summary.lua
ClassicStats.lua