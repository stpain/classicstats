# ClassicStats






=======================================================
ClassicStats Data Merge Tool

This python script is provided for use with the world of warcraft addon ClassicStats.
Its intended use is to merge data from players addon saved variable file(db) and
avoid duplicates only keeping the newer data.

To use this script copy/paste the parent folder to somewhere outside of the warcraft 
game directory and save this file as 'ClassicStats.py' (depending on curse rules it may 
be already saved as a python file), make sure you have python installed on your machine, 
this was written using python 3.7 not sure if it'll work for python 2.x

The script will create 2 files in its containing folder, a db.sqlite3 and a config.ini

MAKE SURE YOU ARE LOGGED OUT AS THE GAME CLIENT WILL 'LOCK' THE ADDON SAVED VARIABLES FILE

https://www.curseforge.com/wow/addons/classicstats