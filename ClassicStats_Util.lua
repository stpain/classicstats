

local _, CS = ...

CS.Util = {}

function CS.Util.GetArgs(...)
    for i=1, select("#", ...) do
        arg = select(i, ...)
        print("|cffC41F3B -EVENT ARGS: |r"..i.." "..tostring(arg))
    end
end

function CS.Util.RgbToPercent(v)
    if type(v) == 'number' then
        return tonumber(v / 256.0)
    end
end

function CS.Util.MakeFrameMovable(frame)
	frame:SetMovable(true)
	frame:EnableMouse(true)
	frame:RegisterForDrag("LeftButton")
	frame:SetScript("OnDragStart", frame.StartMoving)
	frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end

function CS.Util.LockFramePos(frame)
	frame:SetMovable(false)
end

function CS.Util.PrintMessage(msg)
    DEFAULT_CHAT_FRAME:AddMessage("|cff0070DEClassic Stats: |r"..msg)
end

function CS.Util.DebugMessage(msg)
    if ClassicStatsSettings['Debug'] == true then
        DEFAULT_CHAT_FRAME:AddMessage("|cffC41F3B -DEBUG: |r"..msg)
    end
end

function CS.Util.GuildExists()
    local guildName, rankName, rankIndex, _ = GetGuildInfo('player')
    if IsInGuild() and guildName then
        return true
    else
        return false
    end
end

function CS.Util.InsertOrUpdateCharacter(key, data)
    if ClassicStatsDb[key] == nil then
        ClassicStatsDb[key] = data
    else
        local i, j, old, new = 1, 1, {}, {}
        local keys = { [1] = 'Captured', [2] = 'Name', [3] = 'Realm', [4] = 'Gender', [5] = 'Race', [6] = 'Class', [7] = 'GUID', [8] = 'Level' }
        
        for d in string.gmatch(data, '([^,]+)') do
            new[keys[i]] = d
            i = i + 1
        end

        for d in string.gmatch(ClassicStatsDb[key], '([^,]+)') do
            old[keys[j]] = d
            j = j + 1
        end

        if new['Level'] > old['Level'] then
            ClassicStatsDb[key] = data
        end

    end
end