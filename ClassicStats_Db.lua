

local _, CS = ...

CS.Db = {}

CS.Db.ClassColours = {
	DRUID = { r = 1.00, g = 0.49, b = 0.04, fs = '|cffFF7D0A' },
	HUNTER = { r = 0.67, g = 0.83, b = 0.45, fs = '|cffABD473' },
	MAGE = { r = 0.25, g = 0.78, b = 0.92, fs = '|cff40C7EB' },
	PALADIN = { r = 0.96, g = 0.55, b = 0.73, fs = '|cffF58CBA' },
	PRIEST = { r = 1.00, g = 1.00, b = 1.00, fs = '|cffFFFFFF' },
	ROGUE = { r = 1.00, g = 0.96, b = 0.41, fs = '|cffFFF569' },
	SHAMAN = { r = 0.00, g = 0.44, b = 0.87, fs = '|cff0070DE' },
	WARLOCK = { r = 0.53, g = 0.53, b =	0.93, fs = '|cff8787ED' },
	WARRIOR = { r = 0.78, g = 0.61, b = 0.43, fs = '|cffC79C6E' },
	Total = { r = 1.00, g = 1.00, b = 1.00, fs = '|cffFFFFFF' },
}

CS.Db.RaceColours = {
	HUMAN = { r = 0.00, g = 0.44, b = 0.87, fs = '|cffFF7D0A' },
	NIGHTELF = { r = 0.00, g = 0.44, b = 0.87, fs = '|cffABD473' },
	DWARF = { r = 0.00, g = 0.44, b = 0.87, fs = '|cff40C7EB' },
	GNOME = { r = 0.00, g = 0.44, b = 0.87, fs = '|cffF58CBA' },
	ORC = { r = 0.77, g = 0.12, b = 0.23, fs = '|cffFFFFFF' },
	TROLL = { r = 0.77, g = 0.12, b = 0.23, fs = '|cffFFF569' },
	TAUREN = { r = 0.77, g = 0.12, b = 0.23, fs = '|cff0070DE' },
	--UNDEAD = { r = 0.00, g = 0.44, b = 0.87, fs = '|cff8787ED' },
	SCOURGE = { r = 0.77, g = 0.12, b = 0.23, fs = '|cffC79C6E' },
	Total = { r = 1.00, g = 1.00, b = 1.00, fs = '|cffFFFFFF' },
}

CS.Db.ClassCount = {
	{ Class = 'DRUID', Count = 0 },
	{ Class = 'HUNTER', Count = 0 },
	{ Class = 'MAGE', Count = 0 },
	{ Class = 'PALADIN', Count = 0 },
	{ Class = 'PRIEST', Count = 0 },
	{ Class = 'ROGUE', Count = 0 },
	{ Class = 'SHAMAN', Count = 0 },
	{ Class = 'WARLOCK', Count = 0},
	{ Class = 'WARRIOR', Count  = 0 },
	{ Class = 'Total', Count  = 0 },
}

CS.Db.ClassIcons = {
    DRUID = "Interface/AddOns/ClassicStats/ClassIcons/DRUID",
    HUNTER = "Interface/AddOns/ClassicStats/ClassIcons/HUNTER",
    MAGE = "Interface/AddOns/ClassicStats/ClassIcons/MAGE",
    PALADIN = "Interface/AddOns/ClassicStats/ClassIcons/PALADIN",
    PRIEST = "Interface/AddOns/ClassicStats/ClassIcons/PRIEST",
    ROGUE = "Interface/AddOns/ClassicStats/ClassIcons/ROGUE",
    SHAMAN = "Interface/AddOns/ClassicStats/ClassIcons/SHAMAN",
    WARLOCK = "Interface/AddOns/ClassicStats/ClassIcons/WARLOCK",
    WARRIOR = "Interface/AddOns/ClassicStats/ClassIcons/WARRIOR",
}

CS.Db.RaceCount = {
    { Race = 'HUMAN', Count = 0 },
    { Race = 'DWARF', Count = 0 },
    { Race = 'NIGHTELF', Count = 0 },
    { Race = 'GNOME', Count = 0 },
    { Race = 'ORC', Count = 0 },
    { Race = 'UNDEAD', Count = 0 },
    { Race = 'SCOURGE', Count = 0 },
    { Race = 'TAUREN', Count = 0 },
    { Race = 'TROLL', Count = 0},
    { Race = 'Total', Count = 0 }
}

CS.Db.RaceIcons = {
    FEMALE = {
        NIGHTELF = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/NIGHTELF",
        HUMAN = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/HUMAN",
        GNOME = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/GNOME",
        DWARF = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/DWARF",
        ORC = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/ORC",
        TROLL = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/TROLL",
        SCOURGE = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/UNDEAD", --not sure which they are returned as from api
        UNDEAD = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/UNDEAD",
        TAUREN = "Interface/AddOns/ClassicStats/RaceIcons/FEMALE/TAUREN",
    },
    MALE = {
        NIGHTELF = "Interface/AddOns/ClassicStats/RaceIcons/MALE/NIGHTELF",
        HUMAN = "Interface/AddOns/ClassicStats/RaceIcons/MALE/HUMAN",
        GNOME = "Interface/AddOns/ClassicStats/RaceIcons/MALE/GNOME",
        DWARF = "Interface/AddOns/ClassicStats/RaceIcons/MALE/DWARF",
        ORC = "Interface/AddOns/ClassicStats/RaceIconsMALE/ORC",
        TROLL = "Interface/AddOns/ClassicStats/RaceIcons/MALE/TROLL",
        SCOURGE = "Interface/AddOns/ClassicStats/RaceIcons/MALE/UNDEAD", --not sure which they are returned as from api
        UNDEAD = "Interface/AddOns/ClassicStats/RaceIcons/MALE/UNDEAD",
        TAUREN = "Interface/AddOns/ClassicStats/RaceIcons/MALE/TAUREN",
    }
}