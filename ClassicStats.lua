

local _, CS = ...

CS.Vars = { AddonName = 'ClassicStats', Debug = true, Capture = false }

SLASH_CLASSICSTATS1 = '/classicstats'
SlashCmdList['CLASSICSTATS'] = function(msg)

    if msg == 'help' then
        for k, v in pairs(CS.Summary.Realms) do
            print(k, v)
        end

    elseif msg == 'config' then
        if CS.ConfigFrame:IsVisible() then
            CS.ConfigFrame:Hide()
        else
            CS.ConfigFrame:Show()
        end

    elseif msg == 'debug-ON' then
        CS.Vars.Debug = true
        DEFAULT_CHAT_FRAME:AddMessage("|cffC41F3BCS DEBUG: |r"..'debug is on!')

    elseif msg == 'debug-OFF' then
        CS.Vars.Debug = false
        DEFAULT_CHAT_FRAME:AddMessage("|cffC41F3BCS DEBUG: |r"..'debug is off!')

    end
end

CS.EventsFrame = CreateFrame("FRAME", "ClassicStatsEventFrame", UIParent)
CS.EventsFrame:RegisterEvent("ADDON_LOADED")
--CS.EventsFrame:RegisterEvent("CHAT_MSG_FILTERED")
--CS.EventsFrame:RegisterEvent("CHAT_MSG_GUILD")
--CS.EventsFrame:RegisterEvent("CHAT_MSG_PARTY")
CS.EventsFrame:RegisterEvent("CHAT_MSG_CHANNEL")
CS.EventsFrame:RegisterEvent("UPDATE_MOUSEOVER_UNIT")
CS.EventsFrame:RegisterEvent("GUILD_ROSTER_UPDATE")

CS.ConfigFrame = CreateFrame("FRAME", "ClassicStatsConfigFrame", UIParent)
CS.ConfigFrame:SetBackdrop({ bgFile = "Interface/Tooltips/UI-Tooltip-Background", edgeFile = "interface/dialogframe/ui-dialogbox-border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
CS.ConfigFrame:SetBackdropColor(0, 0, 0, 0.9)
CS.ConfigFrame:SetSize(260, 180)
CS.ConfigFrame:SetPoint("CENTER", 0, 0)
CS.ConfigFrame:Hide()

CS.ConfigFrame.HeaderText = CS.ConfigFrame:CreateFontString("ClassicStatsConfigFrame_Header", "OVERLAY", "GameFontNormal")
CS.ConfigFrame.HeaderText:SetPoint("TOP", 0, -10)
CS.ConfigFrame.HeaderText:SetFont("Fonts\\FRIZQT__.TTF", 14)
--CS.ConfigFrame.HeaderText:SetTextColor(1,1,1,1)
CS.ConfigFrame.HeaderText:SetText("Classic Stats Config")

CS.ConfigFrame.EventsSubHeader = CS.ConfigFrame:CreateFontString("ClassicStatsConfigFrame_Header", "OVERLAY", "GameFontNormal")
CS.ConfigFrame.EventsSubHeader:SetPoint("TOPLEFT", 12, -40)
CS.ConfigFrame.EventsSubHeader:SetFont("Fonts\\FRIZQT__.TTF", 11.5)
CS.ConfigFrame.EventsSubHeader:SetTextColor(1,1,1,1)
CS.ConfigFrame.EventsSubHeader:SetText("Capture Events")

CS.ConfigFrame.EnableMouseOver = CreateFrame("CheckButton", 'CSConfigFrameEnableMouseOverCheckButton', CS.ConfigFrame, "ChatConfigCheckButtonTemplate")
CS.ConfigFrame.EnableMouseOver:SetPoint('TOPLEFT', 10, -60)
CSConfigFrameEnableMouseOverCheckButtonText:SetText('UPDATE_MOUSEOVER_UNIT')
--CS.ConfigFrame.EnableMouseOver:SetChecked(true)
CS.ConfigFrame.EnableMouseOver:SetScript('OnClick', function(self) ClassicStatsSettings['CaptureMouseOver'] = self:GetChecked() end)
CS.ConfigFrame.EnableMouseOver.tooltip = 'Captures data whenever you mouse over a player - |cffC41F3Bcaptures all data|r.'

CS.ConfigFrame.EnableChatMsgChannel = CreateFrame("CheckButton", 'CSConfigFrameEnableChatMsgChannelCheckButton', CS.ConfigFrame, "ChatConfigCheckButtonTemplate")
CS.ConfigFrame.EnableChatMsgChannel:SetPoint('TOPLEFT', 10, -80)
CSConfigFrameEnableChatMsgChannelCheckButtonText:SetText('CHAT_MSG_CHANNEL')
--CS.ConfigFrame.EnableChatMsgChannel:SetChecked(true)
CS.ConfigFrame.EnableChatMsgChannel:SetScript('OnClick', function(self) ClassicStatsSettings['CaptureChatMsgChannel'] = self:GetChecked() end)
CS.ConfigFrame.EnableChatMsgChannel.tooltip = 'Captures data from the chat window whenever players send messages - |cffC41F3BDOES NOT capture player level|r.'

CS.ConfigFrame.EnableGuildRoster = CreateFrame("CheckButton", 'CSConfigFrameEnableGuildRosterCheckButton', CS.ConfigFrame, "ChatConfigCheckButtonTemplate")
CS.ConfigFrame.EnableGuildRoster:SetPoint('TOPLEFT', 10, -100)
CSConfigFrameEnableGuildRosterCheckButtonText:SetText('GUILD_ROSTER_UPDATE')
--CS.ConfigFrame.EnableGuildRoster:SetChecked(true)
CS.ConfigFrame.EnableGuildRoster:SetScript('OnClick', function(self) ClassicStatsSettings['CaptureGuildRoster'] = self:GetChecked() end)
CS.ConfigFrame.EnableGuildRoster.tooltip = 'Captures data from the chat window whenever players send messages - |cffC41F3Bcaptures all data|r.'


CS.ConfigFrame.DebugCheckBox = CreateFrame("CheckButton", 'CSConfigFrameDebugCheckBoxCheckButton', CS.ConfigFrame, "ChatConfigCheckButtonTemplate")
CS.ConfigFrame.DebugCheckBox:SetPoint('TOPLEFT', 10, -140)
CSConfigFrameDebugCheckBoxCheckButtonText:SetText('Debug')
--CS.ConfigFrame.DebugCheckBox:SetChecked(true)
CS.ConfigFrame.DebugCheckBox:SetScript('OnClick', function(self) ClassicStatsSettings['Debug'] = self:GetChecked() end)
CS.ConfigFrame.DebugCheckBox.tooltip = 'Prinbt debug info.'


function CS.CreateMinimapButton()
    local ldb = LibStub("LibDataBroker-1.1")
    CS.MinimapButton = ldb:NewDataObject(CS.Vars.AddonName, {
        type = "data source",
        icon = 130781,-- 133741,
        OnClick = function(self, button)
            if button == "LeftButton" then
                -- Standard workaround call OpenToCategory twice
                -- https://www.wowinterface.com/forums/showpost.php?p=319664&postcount=2
                --InterfaceOptionsFrame_OpenToCategory(CS.Vars.AddonName)
                --InterfaceOptionsFrame_OpenToCategory(CS.Vars.AddonName)

                if CS.Summary.MainFrame:IsVisible() then
                    CS.Summary.MainFrame:Hide()
                else
                    CS.Summary.MainFrame:Show()
                end                
            else
                if CS.ConfigFrame:IsVisible() then
                    CS.ConfigFrame:Hide()
                else
                    CS.ConfigFrame:Show()
                end
            end
        end,
        OnTooltipShow = function(tooltip)
            if not tooltip or not tooltip.AddLine then return end
            tooltip:AddLine("|cff0070DE"..CS.Vars.AddonName)
            tooltip:AddLine("|cffFFFFFFLeft click:|r View Summary")
            tooltip:AddLine("|cffFFFFFFRight click:|r Config")
        end,
    })

    CS.MinimapIcon = LibStub("LibDBIcon-1.0")
    if not ClassicStatsSettings.MinimapButton then ClassicStatsSettings.MinimapButton = {} end
    CS.MinimapIcon:Register(CS.Vars.AddonName, CS.MinimapButton, ClassicStatsSettings.MinimapButton)
end

function CS.DelayedLoad()

    CS.Vars.Capture = true

    CS.ConfigFrame.EnableMouseOver:SetChecked(ClassicStatsSettings["CaptureMouseOver"])
    CS.ConfigFrame.EnableChatMsgChannel:SetChecked(ClassicStatsSettings["CaptureChatMsgChannel"])
    CS.ConfigFrame.EnableGuildRoster:SetChecked(ClassicStatsSettings["CaptureGuildRoster"])
    CS.ConfigFrame.DebugCheckBox:SetChecked(ClassicStatsSettings["Debug"])

    
    CS.Summary.RibbonFrame.RealmDropDown_Init()

end

function CS.OnEvent(self, event, ...)

    if event == "ADDON_LOADED" and select(1, ...) == "ClassicStats" then
        CS.Util.PrintMessage('addon loaded, use [/classicstats config] to adjust settings')
        
        if ClassicStatsDb == nil then
            ClassicStatsDb = {}
        end

        if ClassicStatsSettings == nil then
            ClassicStatsSettings = { CaptureGuildRoster = true, CaptureChatMsgChannel = true, CaptureMouseOver = true, Debug = false, MinimapButton = {} }
        end       

        CS.RealmName = GetRealmName()
        CS.CreateMinimapButton()

        C_Timer.After(1, function() CS.DelayedLoad() end)

    end


    ---TODO: update guid key to use a guid+realm format to keep uniqueness
    if event == "UPDATE_MOUSEOVER_UNIT" then
        if CS.Vars.Capture == true and UnitIsPlayer('mouseover') then
            if ClassicStatsSettings['CaptureMouseOver'] == true then
                local gender, race, class = nil, nil, nil
                local guid = UnitGUID('mouseover')
                local level = UnitLevel('mouseover')
                local _, classId, _, raceId, genderId, name, realm = GetPlayerInfoByGUID(guid)
                if genderId == 2 then
                    gender = 'MALE'
                elseif genderId == 3 then
                    gender = 'FEMALE'
                end 
                race = string.upper(raceId)
                class = string.upper(classId)
                CS.Util.DebugMessage(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                ClassicStatsDb[tostring(CS.RealmName..';'..guid)] = tostring(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                --if not ClassicStatsDb[tostring(CS.RealmName..';'..guid)] then
                    --ClassicStatsDb[tostring(CS.RealmName..';'..guid)] = tostring(name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                --end
                --local key, data = tostring(CS.RealmName..';'..guid), tostring(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                --CS.Util.InsertOrUpdateCharacter(key, data)
            end
        end
    end

    if event == "GUILD_ROSTER_UPDATE" then
        if CS.Util.GuildExists() then
            local TotalMembers, OnlineMemebrs, _ = GetNumGuildMembers()
            for i = 1, TotalMembers do
                local _name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, class, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid = GetGuildRosterInfo(i)
                local race, gender = nil, nil
                local _, classId, _, raceId, genderId, name, realm = GetPlayerInfoByGUID(guid)
                if genderId == 2 then
                    gender = 'MALE'
                elseif genderId == 3 then
                    gender = 'FEMALE'
                end
                if raceId == nil and classId == nil then
                    CS.Util.DebugMessage(_name..' caused error in guild scan')
                else
                    race = string.upper(raceId)
                    class = string.upper(classId)
                    CS.Util.DebugMessage(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                    --local key, data = tostring(CS.RealmName..';'..guid), tostring(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                    ClassicStatsDb[tostring(CS.RealmName..';'..guid)] = tostring(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                    --CS.Util.InsertOrUpdateCharacter(key, data)
                end                               
            end
        end
    end

    if event == "CHAT_MSG_CHANNEL" then
        if CS.Vars.Capture == true then
            if ClassicStatsSettings['CaptureChatMsgChannel'] == true then
                local guid = select(12, ...)
                local gender, race, class, level = nil, nil, nil, 0
                local _, classId, _, raceId, genderId, name, realm = GetPlayerInfoByGUID(guid)
                if genderId == 2 then
                    gender = 'MALE'
                elseif genderId == 3 then
                    gender = 'FEMALE'
                end 
                race = string.upper(raceId)
                class = string.upper(classId)
                CS.Util.DebugMessage(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                ClassicStatsDb[tostring(CS.RealmName..';'..guid)] = tostring(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                --if not ClassicStatsDb[tostring(CS.RealmName..';'..guid)] then
                    --ClassicStatsDb[tostring(CS.RealmName..';'..guid)] = tostring(name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                --end
                --local key, data = tostring(CS.RealmName..';'..guid), tostring(GetServerTime()..','..name..','..CS.RealmName..','..gender..','..race..','..class..','..guid..','..level)
                --CS.Util.InsertOrUpdateCharacter(key, data)
            end
        end
    end

end


CS.EventsFrame:SetScript("OnEvent", CS.OnEvent)