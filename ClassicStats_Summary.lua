

local _, CS = ...

CS.Summary = { Realms = {}, Characters = {}, SelectedRealm = GetRealmName(), CharacterCount_Class = 0, CharacterCount_Race = 0 }

CS.Summary.Config = {
    FactionBarHeight = 30.0,
    FactionBarWidth = 200.0,
    FactionBarPosY = -45.0,
    FactionIconSize = 40.0,
    FactionIconPosY = -35.0,
    FactionIconPosX = 130.0,
}

CS.Summary.ClassIDs = { 'WARRIOR', 'DRUID', 'PALADIN', 'PRIEST', 'SHAMAN', 'HUNTER', 'MAGE', 'ROGUE', 'WARLOCK', 'Total' }
CS.Summary.RaceIDs = { 'HUMAN', 'DWARF', 'NIGHTELF', 'GNOME', 'ORC', 'TAUREN', 'TROLL', 'SCOURGE', 'Total' }

CS.Summary.MainFrame = CreateFrame("FRAME", "ClassicStatsSummary_MainFrame", UIParent)
CS.Summary.MainFrame:SetBackdrop({ edgeFile = "interface/dialogframe/ui-dialogbox-border", tile = true, tileSize = 16, edgeSize = 20, insets = { left = 4, right = 4, top = 4, bottom = 4 }});
CS.Summary.MainFrame:SetBackdropColor(0, 0, 0, 0.9)
CS.Summary.MainFrame:SetSize(800, 300)
CS.Summary.MainFrame:SetPoint("CENTER", 0, 0)
CS.Summary.MainFrame:Hide()


CS.Summary.RibbonFrame = CreateFrame("FRAME", "$parent_RaceInfoFrame", CS.Summary.MainFrame)
CS.Summary.RibbonFrame:SetPoint("TOPLEFT", 6, -6)
CS.Summary.RibbonFrame:SetSize(788, 84)
CS.Summary.RibbonFrame.texture = CS.Summary.RibbonFrame:CreateTexture("$parent_Texture", "BACKGROUND")
CS.Summary.RibbonFrame.texture:SetAllPoints(CS.Summary.RibbonFrame)
CS.Summary.RibbonFrame.texture:SetColorTexture(CS.Util.RgbToPercent(10), CS.Util.RgbToPercent(12), CS.Util.RgbToPercent(12), 0.8)

CS.Summary.RibbonFrame.HeaderText = CS.Summary.RibbonFrame:CreateFontString("$parent_HeaderText", "OVERLAY", "GameFontNormal")
CS.Summary.RibbonFrame.HeaderText:SetPoint("TOP", 0, -5)
CS.Summary.RibbonFrame.HeaderText:SetFont("Fonts\\FRIZQT__.TTF", 16)
CS.Summary.RibbonFrame.HeaderText:SetText('Classic Stats - Realm Summary')

CS.Summary.RibbonFrame.AllianceIcon = CS.Summary.RibbonFrame:CreateTexture("$parent_AllianceIcon", "ARTWORK")
CS.Summary.RibbonFrame.AllianceIcon:SetPoint("TOP", CS.Summary.Config.FactionIconPosX * -1, CS.Summary.Config.FactionIconPosY)
CS.Summary.RibbonFrame.AllianceIcon:SetSize(CS.Summary.Config.FactionIconSize, CS.Summary.Config.FactionIconSize)
CS.Summary.RibbonFrame.AllianceIcon:SetTexture(136781)

CS.Summary.RibbonFrame.HordeIcon = CS.Summary.RibbonFrame:CreateTexture("$parent_HordeIcon", "ARTWORK")
CS.Summary.RibbonFrame.HordeIcon:SetPoint("TOP", CS.Summary.Config.FactionIconPosX, CS.Summary.Config.FactionIconPosY)
CS.Summary.RibbonFrame.HordeIcon:SetSize(CS.Summary.Config.FactionIconSize, CS.Summary.Config.FactionIconSize)
CS.Summary.RibbonFrame.HordeIcon:SetTexture(136782)

CS.Summary.RibbonFrame.HordeBar = CreateFrame('StatusBar', '$parent_HordeStatusBar', CS.Summary.RibbonFrame)
CS.Summary.RibbonFrame.HordeBar:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
CS.Summary.RibbonFrame.HordeBar:GetStatusBarTexture():SetHorizTile(false)
CS.Summary.RibbonFrame.HordeBar:SetMinMaxValues(0, 100)
CS.Summary.RibbonFrame.HordeBar:SetValue(100)
CS.Summary.RibbonFrame.HordeBar:SetSize(CS.Summary.Config.FactionBarWidth, CS.Summary.Config.FactionBarHeight)
CS.Summary.RibbonFrame.HordeBar:SetPoint('TOP', 0, CS.Summary.Config.FactionBarPosY)
CS.Summary.RibbonFrame.HordeBar:SetStatusBarColor(CS.Db.RaceColours['ORC'].r, CS.Db.RaceColours['ORC'].g, CS.Db.RaceColours['ORC'].b)
CS.Summary.RibbonFrame.HordeBar.text = CS.Summary.RibbonFrame.HordeBar:CreateFontString("$parent_Text", "OVERLAY", "GameFontNormal")
CS.Summary.RibbonFrame.HordeBar.text:SetPoint("TOPRIGHT", -2, 10)
CS.Summary.RibbonFrame.HordeBar.text:SetTextColor(1,1,1,1)
CS.Summary.RibbonFrame.HordeBar.text:SetFont("Fonts\\FRIZQT__.TTF", 9)

CS.Summary.RibbonFrame.AllianceBar = CreateFrame('StatusBar', '$parent_AllianceStatusBar', CS.Summary.RibbonFrame)
CS.Summary.RibbonFrame.AllianceBar:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
CS.Summary.RibbonFrame.AllianceBar:GetStatusBarTexture():SetHorizTile(false)
CS.Summary.RibbonFrame.AllianceBar:SetMinMaxValues(0, 100)
CS.Summary.RibbonFrame.AllianceBar:SetValue(0)
CS.Summary.RibbonFrame.AllianceBar:SetSize(CS.Summary.Config.FactionBarWidth, CS.Summary.Config.FactionBarHeight)
CS.Summary.RibbonFrame.AllianceBar:SetPoint('TOP', 0, CS.Summary.Config.FactionBarPosY)
CS.Summary.RibbonFrame.AllianceBar:SetStatusBarColor(CS.Db.RaceColours['HUMAN'].r, CS.Db.RaceColours['HUMAN'].g, CS.Db.RaceColours['HUMAN'].b)
CS.Summary.RibbonFrame.AllianceBar:SetFrameStrata('HIGH')
CS.Summary.RibbonFrame.AllianceBar.text = CS.Summary.RibbonFrame.AllianceBar:CreateFontString("$parent_Text", "OVERLAY", "GameFontNormal")
CS.Summary.RibbonFrame.AllianceBar.text:SetPoint("TOPLEFT", 2, 10)
CS.Summary.RibbonFrame.AllianceBar.text:SetTextColor(1,1,1,1)
CS.Summary.RibbonFrame.AllianceBar.text:SetFont("Fonts\\FRIZQT__.TTF", 9)

CS.Summary.RibbonFrame.CloseButton = CreateFrame("BUTTON", "$parent_CloseButton", CS.Summary.RibbonFrame, "UIPanelButtonTemplate")
CS.Summary.RibbonFrame.CloseButton:SetPoint("TOPRIGHT", -5, -5)
CS.Summary.RibbonFrame.CloseButton:SetText('Close')
CS.Summary.RibbonFrame.CloseButton:SetSize(40, 22)
CS.Summary.RibbonFrame.CloseButton:SetScript("OnClick", function() CS.Summary.MainFrame:Hide() end)

CS.Summary.RibbonFrame.RefreshButton = CreateFrame("BUTTON", "$parent_RefreshButton", CS.Summary.RibbonFrame, "UIPanelButtonTemplate")
CS.Summary.RibbonFrame.RefreshButton:SetPoint("TOPLEFT", 10, -10)
CS.Summary.RibbonFrame.RefreshButton:SetText('Refresh')
CS.Summary.RibbonFrame.RefreshButton:SetSize(70, 22)
CS.Summary.RibbonFrame.RefreshButton:SetScript("OnClick", function() CS.Summary.RefreshSummaryFrame() end)

CS.Summary.RibbonFrame.RealmDropDown = CreateFrame("FRAME", "$parent_RealmDropDown", CS.Summary.RibbonFrame, "UIDropDownMenuTemplate")
CS.Summary.RibbonFrame.RealmDropDown:SetPoint("TOPLEFT", -5, -40)
CS.Summary.RibbonFrame.RealmDropDown.displayMode = "MENU"
UIDropDownMenu_SetWidth(CS.Summary.RibbonFrame.RealmDropDown, 150)
UIDropDownMenu_SetText(CS.Summary.RibbonFrame.RealmDropDown, 'Realm')
function CS.Summary.RibbonFrame.RealmDropDown_Init()
    CS.Summary.GetRealmsFromDb()
	UIDropDownMenu_Initialize(CS.Summary.RibbonFrame.RealmDropDown, function(self, level, menuList)
		local info = UIDropDownMenu_CreateInfo()
        for k, realm in pairs(CS.Summary.Realms) do
            info.text = realm
            info.arg1 = realm
            info.arg2 = nil						
            info.func = function()
                CS.Summary.RefreshSummaryFrame()
                UIDropDownMenu_SetText(CS.Summary.RibbonFrame.RealmDropDown, realm)
            end
            info.isNotRadio = true
            UIDropDownMenu_AddButton(info)
            --UIDropDownMenu_AddButton({ text = realm})
        end
	end)
end

--class info
CS.Summary.ClassInfo_Frame = CreateFrame("FRAME", "$parent_ClassInfoFrame", CS.Summary.MainFrame)
CS.Summary.ClassInfo_Frame:SetPoint("TOPLEFT", 6, -90)
CS.Summary.ClassInfo_Frame:SetSize(394, 206)
CS.Summary.ClassInfo_Frame.texture = CS.Summary.ClassInfo_Frame:CreateTexture("$parent_Texture", "BACKGROUND")
CS.Summary.ClassInfo_Frame.texture:SetAllPoints(CS.Summary.ClassInfo_Frame)
CS.Summary.ClassInfo_Frame.texture:SetColorTexture(CS.Util.RgbToPercent(21), CS.Util.RgbToPercent(21), CS.Util.RgbToPercent(28), 0.9)

CS.Summary.ClassInfo_Frame:SetScript("OnShow", function(self) 
    CS.Summary.RefreshSummaryFrame()
end)

CS.Summary.ClassInfo_Icon = {}
CS.Summary.ClassInfo_Name = {}
CS.Summary.ClassInfo_Count = {}
CS.Summary.ClassInfo_Percent = {}
CS.Summary.ClassInfo_StatusBars = {}

CS.Summary.ClassSummaryFontSize = 12
CS.Summary.ClassSummaryStatusBarWidth = 100
CS.Summary.ClassSummaryStatusBarHeight = 18
CS.Summary.ClassSummaryVerticalOffset = 6
CS.Summary.ClassSummaryRowHeight = -20
CS.Summary.ClassSummaryIconHeight = 20

for k, v in ipairs(CS.Summary.ClassIDs) do
        CS.Summary.ClassInfo_Icon[k] = CreateFrame("FRAME", "ClassicStatsClassIcon_"..k, CS.Summary.ClassInfo_Frame)
        CS.Summary.ClassInfo_Icon[k]:SetPoint("TOPLEFT", 10, ((k-1) * CS.Summary.ClassSummaryRowHeight) - CS.Summary.ClassSummaryVerticalOffset + 4)
        CS.Summary.ClassInfo_Icon[k]:SetSize(CS.Summary.ClassSummaryIconHeight, CS.Summary.ClassSummaryIconHeight)
        CS.Summary.ClassInfo_Icon[k].texture = CS.Summary.ClassInfo_Icon[k]:CreateTexture("$parent_Texure", "ARTWORK")
        CS.Summary.ClassInfo_Icon[k].texture:SetAllPoints(CS.Summary.ClassInfo_Icon[k])

        CS.Summary.ClassInfo_Name[k] = CS.Summary.ClassInfo_Frame:CreateFontString('CS.SummaryClassInfoName_'..k, 'OVERLAY', 'GameFontNormal')
        CS.Summary.ClassInfo_Name[k]:SetPoint('TOPLEFT', 30, ((k-1) * CS.Summary.ClassSummaryRowHeight) - CS.Summary.ClassSummaryVerticalOffset)
        CS.Summary.ClassInfo_Name[k]:SetText(v)
        CS.Summary.ClassInfo_Name[k]:SetFont("Fonts\\FRIZQT__.TTF", CS.Summary.ClassSummaryFontSize)
        CS.Summary.ClassInfo_Name[k]:SetTextColor(CS.Db.ClassColours[v].r, CS.Db.ClassColours[v].g, CS.Db.ClassColours[v].b)
        
        CS.Summary.ClassInfo_Count[k] = CS.Summary.ClassInfo_Frame:CreateFontString('CS.SummaryClassInfoCount_'..k, 'OVERLAY', 'GameFontNormal')
        CS.Summary.ClassInfo_Count[k]:SetPoint('TOPLEFT', 120, ((k-1) * CS.Summary.ClassSummaryRowHeight) - CS.Summary.ClassSummaryVerticalOffset)
        --CS.Summary.ClassInfo_Count[k]:SetText(v)
        CS.Summary.ClassInfo_Count[k]:SetFont("Fonts\\FRIZQT__.TTF", CS.Summary.ClassSummaryFontSize)
        CS.Summary.ClassInfo_Count[k]:SetTextColor(CS.Db.ClassColours[v].r, CS.Db.ClassColours[v].g, CS.Db.ClassColours[v].b)
        
        CS.Summary.ClassInfo_Percent[k] = CS.Summary.ClassInfo_Frame:CreateFontString('CS.SummaryClassInfoPercent_'..k, 'OVERLAY', 'GameFontNormal')
        CS.Summary.ClassInfo_Percent[k]:SetPoint('TOPLEFT', 180, ((k-1) * CS.Summary.ClassSummaryRowHeight) - CS.Summary.ClassSummaryVerticalOffset)
        --CS.Summary.ClassInfo_Percent[k]:SetText(v)
        CS.Summary.ClassInfo_Percent[k]:SetFont("Fonts\\FRIZQT__.TTF", CS.Summary.ClassSummaryFontSize)
        CS.Summary.ClassInfo_Percent[k]:SetTextColor(CS.Db.ClassColours[v].r, CS.Db.ClassColours[v].g, CS.Db.ClassColours[v].b)
        
        CS.Summary.ClassInfo_StatusBars[k] = CreateFrame('StatusBar', 'CS.SummaryClassInfoStatusBar_'..k, CS.Summary.ClassInfo_Frame)
        CS.Summary.ClassInfo_StatusBars[k]:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
        CS.Summary.ClassInfo_StatusBars[k]:GetStatusBarTexture():SetHorizTile(false)
        CS.Summary.ClassInfo_StatusBars[k]:SetMinMaxValues(0, 100)
        CS.Summary.ClassInfo_StatusBars[k]:SetValue(0)
        CS.Summary.ClassInfo_StatusBars[k]:SetSize(CS.Summary.ClassSummaryStatusBarWidth, CS.Summary.ClassSummaryStatusBarHeight)
        CS.Summary.ClassInfo_StatusBars[k]:SetPoint('TOPLEFT', 250, ((k-1) * CS.Summary.ClassSummaryRowHeight) - (CS.Summary.ClassSummaryVerticalOffset - 2))
        CS.Summary.ClassInfo_StatusBars[k]:SetStatusBarColor(CS.Db.ClassColours[v].r, CS.Db.ClassColours[v].g, CS.Db.ClassColours[v].b)
end







--race info
CS.Summary.RaceInfo_Frame = CreateFrame("FRAME", "$parent_RaceInfoFrame", CS.Summary.MainFrame)
CS.Summary.RaceInfo_Frame:SetPoint("TOPLEFT", 400, -90)
CS.Summary.RaceInfo_Frame:SetSize(394, 206)
CS.Summary.RaceInfo_Frame.texture = CS.Summary.RaceInfo_Frame:CreateTexture("$parent_Texture", "BACKGROUND")
CS.Summary.RaceInfo_Frame.texture:SetAllPoints(CS.Summary.RaceInfo_Frame)
CS.Summary.RaceInfo_Frame.texture:SetColorTexture(CS.Util.RgbToPercent(22), CS.Util.RgbToPercent(19), CS.Util.RgbToPercent(24), 0.9)

CS.Summary.RaceInfo_Frame:SetScript("OnShow", function(self) 
    CS.Summary.RefreshSummaryFrame()
end)

CS.Summary.RaceInfo_Name = {}
CS.Summary.RaceInfo_Count = {}
CS.Summary.RaceInfo_Percent = {}
CS.Summary.RaceInfo_StatusBars = {}

CS.Summary.RaceSummaryFontSize = 12
CS.Summary.RaceSummaryStatusBarWidth = 100
CS.Summary.RaceSummaryStatusBarHeight = 18
CS.Summary.RaceSummaryVerticalOffset = 6
CS.Summary.RaceSummaryRowHeight = -20

for k, v in ipairs(CS.Summary.RaceIDs) do
	CS.Summary.RaceInfo_Name[k] = CS.Summary.RaceInfo_Frame:CreateFontString('PopbotRaceInfoName_'..k, 'OVERLAY', 'GameFontNormal')
	CS.Summary.RaceInfo_Name[k]:SetPoint('TOPLEFT', 10, ((k-1) * CS.Summary.RaceSummaryRowHeight) - CS.Summary.RaceSummaryVerticalOffset)
	CS.Summary.RaceInfo_Name[k]:SetText(v)
	CS.Summary.RaceInfo_Name[k]:SetFont("Fonts\\FRIZQT__.TTF", CS.Summary.RaceSummaryFontSize)
	CS.Summary.RaceInfo_Name[k]:SetTextColor(CS.Db.RaceColours[v].r, CS.Db.RaceColours[v].g, CS.Db.RaceColours[v].b)
	
	CS.Summary.RaceInfo_Count[k] = CS.Summary.RaceInfo_Frame:CreateFontString('PopbotRaceInfoCount_'..k, 'OVERLAY', 'GameFontNormal')
	CS.Summary.RaceInfo_Count[k]:SetPoint('TOPLEFT', 90, ((k-1) * CS.Summary.RaceSummaryRowHeight) - CS.Summary.RaceSummaryVerticalOffset)
	--CS.Summary.RaceInfo_Count[k]:SetText(v)
	CS.Summary.RaceInfo_Count[k]:SetFont("Fonts\\FRIZQT__.TTF", CS.Summary.RaceSummaryFontSize)
	CS.Summary.RaceInfo_Count[k]:SetTextColor(CS.Db.RaceColours[v].r, CS.Db.RaceColours[v].g, CS.Db.RaceColours[v].b)
	
	CS.Summary.RaceInfo_Percent[k] = CS.Summary.RaceInfo_Frame:CreateFontString('PopbotRaceInfoPercent_'..k, 'OVERLAY', 'GameFontNormal')
	CS.Summary.RaceInfo_Percent[k]:SetPoint('TOPLEFT', 150, ((k-1) * CS.Summary.RaceSummaryRowHeight) - CS.Summary.RaceSummaryVerticalOffset)
	--CS.Summary.RaceInfo_Percent[k]:SetText(v)
	CS.Summary.RaceInfo_Percent[k]:SetFont("Fonts\\FRIZQT__.TTF", CS.Summary.RaceSummaryFontSize)
	CS.Summary.RaceInfo_Percent[k]:SetTextColor(CS.Db.RaceColours[v].r, CS.Db.RaceColours[v].g, CS.Db.RaceColours[v].b)
	
	CS.Summary.RaceInfo_StatusBars[k] = CreateFrame('StatusBar', 'PopbotRaceInfoStatusBar_'..k, CS.Summary.RaceInfo_Frame)
	CS.Summary.RaceInfo_StatusBars[k]:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
	CS.Summary.RaceInfo_StatusBars[k]:GetStatusBarTexture():SetHorizTile(false)
	CS.Summary.RaceInfo_StatusBars[k]:SetMinMaxValues(0, 100)
	CS.Summary.RaceInfo_StatusBars[k]:SetValue(0)
	CS.Summary.RaceInfo_StatusBars[k]:SetSize(CS.Summary.RaceSummaryStatusBarWidth, CS.Summary.RaceSummaryStatusBarHeight)
	CS.Summary.RaceInfo_StatusBars[k]:SetPoint('TOPLEFT', 220, ((k-1) * CS.Summary.RaceSummaryRowHeight) - (CS.Summary.RaceSummaryVerticalOffset - 2))
	CS.Summary.RaceInfo_StatusBars[k]:SetStatusBarColor(CS.Db.RaceColours[v].r, CS.Db.RaceColours[v].g, CS.Db.RaceColours[v].b)
end




function CS.Summary.RefreshSummaryFrame()
    CS.Summary.ParseDatabase() 
    CS.Summary.FilterCharacters(CS.Summary.SelectedRealm)
    CS.Summary.RefreshClassFrame()
    CS.Summary.RefreshRaceFrame()
    CS.Summary.RefreshFactionBar()
end




function CS.Summary.ResetClassCount()
    CS.Db.ClassCount = {
        { Class = 'DRUID', Count = 0 },
        { Class = 'HUNTER', Count = 0 },
        { Class = 'MAGE', Count = 0 },
        { Class = 'PALADIN', Count = 0 },
        { Class = 'PRIEST', Count = 0 },
        { Class = 'ROGUE', Count = 0 },
        { Class = 'SHAMAN', Count = 0 },
        { Class = 'WARLOCK', Count = 0},
        { Class = 'WARRIOR', Count  = 0 },
        { Class = 'Total', Count  = 0 },
    }
end

function CS.Summary.ResetRaceCount()
    CS.Db.RaceCount = {
        { Race = 'HUMAN', Count = 0 },
        { Race = 'DWARF', Count = 0 },
        { Race = 'NIGHTELF', Count = 0 },
        { Race = 'GNOME', Count = 0 },
        { Race = 'ORC', Count = 0 },
        --{ Race = 'UNDEAD', Count = 0 },
        { Race = 'SCOURGE', Count = 0 },
        { Race = 'TAUREN', Count = 0 },
        { Race = 'TROLL', Count = 0},
        { Race = 'Total', Count = 0 }
    }
end

--- reads the db file and converts comma seperated strings into serialized lua table
function CS.Summary.ParseDatabase()
    CS.Summary.Characters = nil
    CS.Summary.Characters = {}    
    if ClassicStatsDb ~= nil then
        for k, character in pairs(ClassicStatsDb) do
            local i, characterData = 1, {}
            local keys = { [1] = 'Captured', [2] = 'Name', [3] = 'Realm', [4] = 'Gender', [5] = 'Race', [6] = 'Class', [7] = 'GUID', [8] = 'Level' }
            for d in string.gmatch(character, '([^,]+)') do
                characterData[keys[i]] = d
                i = i + 1                
            end
            --table.insert(CS.Summary.Characters, { Name = characterData[1], Realm = characterData[2], Gender = characterData[3], Race = characterData[4], Class = characterData[5], GUID = characterData[6], Level = characterData[7] })
            table.insert(CS.Summary.Characters, { Captured = characterData['Captured'], Name = characterData['Name'], Realm = characterData['Realm'], Gender = characterData['Gender'], Race = characterData['Race'], Class = characterData['Class'], GUID = characterData['GUID'], Level = characterData['Level'] })
        end   
    else
    
    end
end

function CS.Summary.GetRealmsFromDb()
    CS.Summary.ParseDatabase()
    CS.Summary.Realms = nil
    CS.Summary.Realms = {}
    if CS.Summary.Characters == nil then
        CS.Util.PrintMessage('database returns nil, unable to filter characters')
        return
    else
        for k, character in ipairs(CS.Summary.Characters) do
            --print(character.Realm)
            if not CS.Summary.Realms[character.Realm] then
                CS.Summary.Realms[character.Realm] = character.Realm --just setting a value here but its the key i want
            end
        end
    end
end

--- 
function CS.Summary.FilterCharacters(realm) --CS.Summary.SelectedRealm
    if realm == nil then return end
    CS.Summary.ParseDatabase()
    if CS.Summary.Characters == nil then
        CS.Util.PrintMessage('database returns nil, unable to filter characters')
        return
    else
        CS.Summary.CharacterCount_Class = 0
        CS.Summary.CharacterCount_Race = 0
        CS.Summary.ResetClassCount()
        CS.Summary.ResetRaceCount()
        for k, character in ipairs(CS.Summary.Characters) do
            if character.Realm == realm then
                for _, class in ipairs(CS.Db.ClassCount) do
                    if character.Class == class.Class then
                        CS.Summary.CharacterCount_Class = CS.Summary.CharacterCount_Class + 1
                        class.Count = class.Count + 1
                    end
                end
                for _, race in ipairs(CS.Db.RaceCount) do
                    if character.Race == race.Race then
                        CS.Summary.CharacterCount_Race = CS.Summary.CharacterCount_Race + 1
                        race.Count = race.Count + 1
                    end
                end
            end
        end

		for k, v in pairs(CS.Db.ClassCount) do
			if v.Class == 'Total' then
				v.Count = CS.Summary.CharacterCount_Class
			end
		end		
        table.sort(CS.Db.ClassCount, function(a, b) if a.Count ~= nil and b.Count ~= nil then return tonumber(a.Count) > tonumber(b.Count) end end)

		for k, v in pairs(CS.Db.RaceCount) do
			if v.Race == 'Total' then
				v.Count = CS.Summary.CharacterCount_Race
			end
		end		
        table.sort(CS.Db.RaceCount, function(a, b) if a.Count ~= nil and b.Count ~= nil then return tonumber(a.Count) > tonumber(b.Count) end end)        
    end
end

function CS.Summary.RefreshClassFrame()
    for k, class in ipairs(CS.Db.ClassCount) do
        CS.Summary.ClassInfo_Icon[k].texture:SetTexture(CS.Db.ClassIcons[class.Class])
        CS.Summary.ClassInfo_Name[k]:SetText(class.Class)
        CS.Summary.ClassInfo_Name[k]:SetTextColor(CS.Db.ClassColours[class.Class].r, CS.Db.ClassColours[class.Class].g, CS.Db.ClassColours[class.Class].b)
        CS.Summary.ClassInfo_Count[k]:SetText(class.Count)
        CS.Summary.ClassInfo_Count[k]:SetTextColor(CS.Db.ClassColours[class.Class].r, CS.Db.ClassColours[class.Class].g, CS.Db.ClassColours[class.Class].b)
        CS.Summary.ClassInfo_Percent[k]:SetText(string.format("%.2f", ((class.Count/ CS.Summary.CharacterCount_Class) * 100))..'%')
        CS.Summary.ClassInfo_Percent[k]:SetTextColor(CS.Db.ClassColours[class.Class].r, CS.Db.ClassColours[class.Class].g, CS.Db.ClassColours[class.Class].b)
        CS.Summary.ClassInfo_StatusBars[k]:SetValue((class.Count/ CS.Summary.CharacterCount_Class) * 100)
        CS.Summary.ClassInfo_StatusBars[k]:SetStatusBarColor(CS.Db.ClassColours[class.Class].r, CS.Db.ClassColours[class.Class].g, CS.Db.ClassColours[class.Class].b)
    end
end

function CS.Summary.RefreshRaceFrame()
    for k, race in ipairs(CS.Db.RaceCount) do
        CS.Summary.RaceInfo_Name[k]:SetText(race.Race)
        CS.Summary.RaceInfo_Name[k]:SetTextColor(CS.Db.RaceColours[race.Race].r, CS.Db.RaceColours[race.Race].g, CS.Db.RaceColours[race.Race].b)
        CS.Summary.RaceInfo_Count[k]:SetText(race.Count)
        CS.Summary.RaceInfo_Count[k]:SetTextColor(CS.Db.RaceColours[race.Race].r, CS.Db.RaceColours[race.Race].g, CS.Db.RaceColours[race.Race].b)
        CS.Summary.RaceInfo_Percent[k]:SetText(string.format("%.2f", ((race.Count/ CS.Summary.CharacterCount_Race) * 100))..'%')
        CS.Summary.RaceInfo_Percent[k]:SetTextColor(CS.Db.RaceColours[race.Race].r, CS.Db.RaceColours[race.Race].g, CS.Db.RaceColours[race.Race].b)
        CS.Summary.RaceInfo_StatusBars[k]:SetValue((race.Count/ CS.Summary.CharacterCount_Race) * 100)
        CS.Summary.RaceInfo_StatusBars[k]:SetStatusBarColor(CS.Db.RaceColours[race.Race].r, CS.Db.RaceColours[race.Race].g, CS.Db.RaceColours[race.Race].b)
    end
end

function CS.Summary.RefreshFactionBar()

    local ally, horde = 0, 0
    for k, v in pairs(CS.Summary.Characters) do
        if v.Race == 'HUMAN' then
            ally = ally + 1
        elseif v.Race == 'NIGHTELF' then
            ally = ally + 1
        elseif v.Race == 'DWARF' then
            ally = ally + 1
        elseif v.Race == 'GNOME' then
            ally = ally + 1
        elseif v.Race == 'ORC' then
            horde = horde + 1
        elseif v.Race == 'SCOURGE' then
            horde = horde + 1
        elseif v.Race == 'TROLL' then
            horde = horde + 1
        elseif v.Race == 'TAUREN' then
            horde = horde + 1
        end
    end

    local horde = ((tonumber(horde) / tonumber(#CS.Summary.Characters)) * 100.0)
    local ally = ((tonumber(ally) / tonumber(#CS.Summary.Characters)) * 100.0)
    CS.Summary.RibbonFrame.AllianceBar:SetValue(ally)

    CS.Summary.RibbonFrame.AllianceBar.text:SetText(string.format("%.2f", ally)..'%')
    CS.Summary.RibbonFrame.HordeBar.text:SetText(string.format("%.2f", horde)..'%')

end
